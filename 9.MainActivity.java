package mayurbhangale.simplecalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    double value1, value2, result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView resultField = (TextView) findViewById(R.id.resultTV);
        final EditText firstInput = (EditText) findViewById(R.id.firstValET);
        final EditText secondInput = (EditText) findViewById(R.id.secondValET);

        Button addBtn = (Button) findViewById(R.id.plusBtn);
        Button minusbtn = (Button) findViewById(R.id.minusBtn);
        Button sinBtn = (Button) findViewById(R.id.button7);
        Button squareRtBtn = (Button) findViewById(R.id.button8);

        minusbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value1 = Float.parseFloat(firstInput.getText().toString());
                value2 = Float.parseFloat(secondInput.getText().toString());
                result = value1 - value2;
                resultField.setText("subtraction is " + result);
            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value1 = Float.parseFloat(firstInput.getText().toString());
                value2 = Float.parseFloat(secondInput.getText().toString());
                result = value1 + value2;
                resultField.setText("Adddition is" + result);
            }
        });
        sinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value1 = Float.parseFloat(firstInput.getText().toString());
                result = Math.sin(Math.toRadians(value1));
                resultField.setText("Sin("+value1 +") = "+ result);
            }
        });

        squareRtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value1 = Float.parseFloat(firstInput.getText().toString());
                result = Math.sqrt(Math.toRadians(value1));
                resultField.setText("Square Root("+value1 +") = "+ result);
            }
        });
    }
}
