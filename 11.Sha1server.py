# server.py
import socket
import hashlib

# create a socket object
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# get local machine name
host = '127.0.0.1'

port = 9999

# bind to the port
serversocket.bind((host, port))

# queue up to 5 requests
serversocket.listen(5)

while True:
    # establish a connection
    clientsocket, addr = serversocket.accept()

    print("Got a connection from %s" % str(addr))
    hash_object = hashlib.sha1(b'Hello World')
    hex_dig = hash_object.hexdigest()
    clientsocket.send(hex_dig.encode('ascii'))
    clientsocket.close()

#run Sha1server.py then run Sha1Client.py