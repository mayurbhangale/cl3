from flask import Flask
from difflib import SequenceMatcher

app = Flask(__name__)

@app.route('/')
def hello_world():
    file1Data = "hello world"
    file2Data = "hello world"
    similarity_ratio = SequenceMatcher(None, file1Data, file2Data).ratio()
    return str(similarity_ratio)

if __name__ == '__main__':
    app.run()
