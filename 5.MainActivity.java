package mayurbhangale.trigonometry;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    double value;
    double result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText inputEt =(EditText) findViewById(R.id.edit_input);
        final TextView resultTv = (TextView) findViewById(R.id.tv_result);
        final Button sine = (Button) findViewById(R.id.btn_sine);
        final Button cos = (Button) findViewById(R.id.btn_cos);
        final Button tan = (Button) findViewById(R.id.btn_tan);

        sine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value = Float.parseFloat(inputEt.getText().toString());
                result = Math.sin(Math.toRadians(value));
                resultTv.setText("Sin("+value +") = "+ result);
            }
        });

        cos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value = Float.parseFloat(inputEt.getText().toString());
                result = Math.cos(Math.toRadians(value));
                resultTv.setText("Cos("+value+") = "+ result);
            }
        });

        tan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value = Float.parseFloat(inputEt.getText().toString());
                result = Math.tan(Math.toRadians(value));
                resultTv.setText("Tan("+value+") = "+ result);
            }
        });
    }
}
