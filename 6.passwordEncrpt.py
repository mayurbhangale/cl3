key = 'abcdefghijklmnopqrstuvwxyz'


def encrypt(n, plaintext):
    result = ''

    for l in plaintext.lower():
        try:
            i = (key.index(l) + n) % 26
            result += key[i]
        except ValueError:
            result += l

    return result.lower()


def decrypt(n, ciphertext):
    result = ''

    for l in ciphertext:
        try:
            i = (key.index(l) - n) % 26
            result += key[i]
        except ValueError:
            result += l

    return result


def show_result(plaintext, n):
    encrypted = encrypt(n, plaintext)
    decrypted = decrypt(n, encrypted)

    print('Rotation: ' + str(n))
    print('Plaintext Password: ' + plaintext)
    print('Encrytped Password: ' + encrypted)
    print('Decrytped Password: ' + decrypted)

show_result('hello world', 3)
